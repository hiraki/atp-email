#! /usr/bin/bash

echo 'install epel'
sudo yum install -y http://ftp.jaist.ac.jp/pub/Linux/Fedora/epel//7/x86_64/e/epel-release-7-5.noarch.rpm

echo 'install git, ansible'
sudo yum install -y git ansible

echo 'git clone repo'
git clone https://bitbucket.org/hiraki/atp-email atp-email

echo 'cd to ansible dir'
cd atp-email/ansible && ansible-playbook playbook.yml

