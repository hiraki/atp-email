(function(){

  var templateVal = {
    url: '%url%',
    mail: '%mail%',
    firstname: '%firstname%',
    lastname: '%lastname%',
    division1: '%division1%',
    division2: '%division2%'
  };

  function append2templateText(appendText){
    var area = $("#template-text");
    var splitter = area.get(0).selectionStart;
    var text = area.val();
    var leftText = text.substr(0, splitter);
    var rightText = text.substr(splitter, text.length);
    var textLength = appendText.length;
    $("#template-text").val(leftText + appendText + rightText);
    area.get(0).selectionStart = area.get(0).selectionEnd = splitter + textLength;
    area.focus();
  }

  $(document).on("click", "#button-add-click-url", function(){
    var textFormat = $(":input[type=radio][name=format]:checked").val();
    switch(textFormat){
      case 'plain':
        return append2templateText(templateVal.url);
        break;
      case 'HTML':
        return append2templateText('<a href="' + templateVal.url + '">' + '【任意のURL】' + '</a>');
        break;
      default:
    }
  });

  $(document).on("click", "#button-add-open-tag", function(){
    append2templateText('<img src="' + templateVal.mail + '" />');
  });

  $(document).on("click", "#button-add-firstname", function(){
    append2templateText(templateVal.firstname);
  });

  $(document).on("click", "#button-add-lastname", function(){
    append2templateText(templateVal.lastname);
  });

  $(document).on("click", "#button-add-division1", function(){
    append2templateText(templateVal.division1);
  });

  $(document).on("click", "#button-add-division2", function(){
    append2templateText(templateVal.division2);
  });

  $(document).on("click", ":input[name=hasAttachment]:checked", function(){
    if($(this).val() === 'true'){
      $("#attachment-select").removeAttr('disabled');
    }else{
      $("#attachment-select").attr('disabled', 'disabled');
    }
  });

  $(document).on("click", ":input[name=schedule_type]", function(){
    switch($(this).val()){
      case 'at':
        $("input[name=config_at").removeAttr('disabled');
        $("select[name=config_cron]").attr('disabled', 'disabled');
        break;
      case 'cron':
        $("select[name=config_cron").removeAttr('disabled');
        $("input[name=config_at]").attr('disabled', 'disabled');
        break;
      default:
    }
  });

  $(document).on("click", ":input[type=radio][name=format]", function(){
    switch($(this).val()){
      case 'plain':
        $("#button-add-open-tag").attr('disabled', 'disabled');
        break;
      case 'HTML':
        $("#button-add-open-tag").removeAttr('disabled', 'disabled');
        break;
      default:
    }
  });

  // $("#button-emailgroup-add-submit").on("click", function(){
  //   var form = $("#emailgroup-add-form");
  //   form.attr('action', '/email_group/add');
  //   form.submit();
  // });

  $("#button-checkall").on("click", function(){
    $("input[type=checkbox]").prop({'checked': 'checked'});
  });

  $("#button-uncheckall").on("click", function(){
    $("input[type=checkbox]").prop({'checked': false});
  });

}());
