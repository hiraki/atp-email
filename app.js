var config = require('./config.json');

var express = require('express'),
    app = express(),
    ejs = require('ejs'),
    multer = require('multer'),
    upload = multer({dest: 'uploads/'}),
    bodyParser = require('body-parser');

var scenario = require('./script/controller/scenario.js'),
    schedule = require('./script/controller/schedule.js'),
    counter = require('./script/controller/counter.js'),
    template = require('./script/controller/template.js'),
    statistics = require('./script/controller/statistics.js'),
    emailGroup = require('./script/controller/email-group.js'),
    attachment = require('./script/controller/attachment.js'),
    user = require('./script/controller/user.js'),
    dashboard = require('./script/controller/dashboard.js'),
    emailClient = require('./script/controller/email-client.js'),
    mail = require('./script/mail.js');

app.use(bodyParser({
  extended: false,
  parameterLimit: 10000,
  limit: 1024 * 1024 * 10
}));

app.use(express.static('public'));
app.engine('ejs', ejs.renderFile);

app.get('/', dashboard.show);

app.get('/template/operate/:cmd/:id', template.operate.get);
app.post('/template/operate/:cmd', template.operate.post);

app.post('/email_group/add', emailGroup.add);
app.get('/email_group/mgmt', emailGroup.mgmtAll);
app.get('/email_group/:name/mgmt', emailGroup.mgmt);
app.post('/email_group/add/confirm', emailGroup.addConfirm);

app.get('/schedule/mgmt', schedule.mgmt);
app.post('/schedule/add', schedule.add);
app.post('/schedule/:type/:scenarioId', schedule.operate);

app.get('/template/mgmt', template.mgmt);

app.get('/attachment/mgmt', attachment.mgmt);

app.get('/statistics/:scenario/mgmt', statistics.mgmt);
app.get('/statistics/download/:fileName', statistics.download);

app.post('/scenario/add', scenario.add);
app.get('/scenario/mgmt', scenario.mgmt);
app.post('/scenario/send', scenario.send);
app.get('/scenario/:id/send/confirm', scenario.sendConfirm);
app.get('/scenario/:id/sent', scenario.sent);

app.post('/attachment/upload', upload.single('attachment'), attachment.upload);
app.get('/attachment/download/:id', attachment.download);

app.get('/user/mgmt', user.mgmt);
app.post('/user/add', user.add);
app.post('/user/queue/add', user.addFromQueue);
app.post('/user/upload', upload.single('emails'), user.queue);

app.get('/config', emailClient.getConfig);
app.post('/config/email_client/modify', emailClient.setConfig);

app.get('/open/mail/:uniqueid/:user', counter.mailAccess);
app.get('/open/url/:uniqueid/:user', counter.urlAccess);
app.get('/open/file/:uniqueid/:user', counter.fileAccess);

app.listen(config.port);
console.log('Server is running at http://' + config.fqdn + ':' + config.port);

