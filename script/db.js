var config = require('../config.json');
var mysql = require('mysql');

module.exports.db = function db(callback) {
  var connection = mysql.createConnection(config.db);

  connection.connect(function(err) {
    if (err) {
      throw err;
    }
    callback(connection);
  });
};

