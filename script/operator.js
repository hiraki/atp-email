var cmds = ['add', 'modify', 'remove', 'duplicate'];

function post(model, cmd, data, callback){
  if(cmds.indexOf(cmd) === -1){
    callback(false);
    return;
  }
  switch(cmd){
    case 'modify':
      model.modify(data, function(){
        callback(true);
      });
      return;
    case 'add':
    case 'duplicate':
      delete data.id;
      model.add(data, function(){
        callback(true);
      });
      return;
    case 'remove':
      model.remove({id: data.id}, function(){
        callback(true);
      });
      return;
  }
};

module.exports.post = post;
