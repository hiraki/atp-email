function getLocalString(d){
  var year  = d.getFullYear(),
      month = d.getMonth()+1,
      date  = d.getDate(),
      hour  = d.getHours(),
      min   = d.getMinutes(),
      sec   = d.getSeconds();
  return [year, month, date].join('/') + ' ' + [hour, min, sec].join(':');
}

function isEmail(addr){
  return addr.match(/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@\[?([\d\w\.-]+)]?$/);
}

module.exports.getLocalString = getLocalString;
module.exports.isEmail = isEmail;
