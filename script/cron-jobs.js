var CronJob = require('cron').CronJob,
    cronFactory = require('./model/cron.js').cronFactory,
    model = {
      cron: cronFactory('cron'),
      at: cronFactory('at')
    };
var cronjobs = {},
    atjobs = {};

function add(options, callback){
  var callback = callback || function(){};
  var type = options.type;
  var job;

  switch(type){
    case 'cron':
      if(cronjobs[options.scenario]){
        callback('SCHEDULE_DUPLICATED');
        return;
      }
      job = new CronJob({
        cronTime: options.config,
        onTick: options.exec,
        start: false,
        timeZone: 'Asia/Tokyo'});
      cronjobs[options.scenario] = job;
      model.cron.add({
        scenario: options.scenario,
        status: 'created',
        config: options.config
      }, function(){
        callback();
      });
      break;

    case 'at':
      if(atjobs[options.scenario]){
        callback('SCHEDULE_DUPLICATED');
        return;
      }
      job = new CronJob(
        options.date,
        function(){
          options.exec();
          delete atjobs[options.scenario];
          changeStatus(options.scenario, type, 'done');
        },
        function(){},
        true,
        'Asia/Tokyo');
      atjobs[options.scenario] = job;
      model.at.add({
        scenario: options.scenario,
        status: 'wait',
        date: options.date
      }, function(){
        callback();
      });
      break;

    default:
      console.log('Invaild type: ' + options.type);
      callback('false');
      return;
  }

  return job;
};

function getJobs(type){
  switch(type){
    case 'cron':
      return cronjobs;
    case 'at':
      return atjobs;
    default:
      return null;
  }
}

function getModel(type){
  switch(type){
    case 'cron':
      return model.cron;
    case 'at':
      return model.at;
    default:
      return null;
  }
}

function remove(id, type, callback){
  var jobs = getJobs(type),
      m = getModel(type);
  if(m !== null && jobs !== null){
    delete jobs[id];
    m.remove(id, callback);
    return true;
  }
  return false;
};

function start(id, type){
  var jobs = getJobs(type);
  if(jobs !== null){
    jobs[id].start();
    changeStatus(id, type, 'running');
    return true;
  }else{
    return false;
  }
};

function stop(id, type){
  var jobs = getJobs(type);
  if(jobs !== null){
    jobs[id].stop();
    changeStatus(id, type, 'stop');
    return true;
  }else{
    return false;
  }
};

function changeStatus(id, type, newStatus, callback){
  callback = callback || function(){};
  var m = getModel(type);
  if(m !== null){
    m.changeStatus(id, type, newStatus, function(result){
      callback(result);
    });
  }
}

function cleardb(){
  model.cron.removeAll();
  model.at.removeAll();
}

module.exports = {
  add: add,
  remove: remove,
  start: start,
  stop: stop
};

cleardb();
