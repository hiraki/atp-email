var path = require('path'),
    model = require('../model/attachments.js');

function upload(req, res){
  var file = {};

  if(!req.file){
    res.render('error.ejs', {
      title: 'エラー',
      errors: [
      { name: 'FILE_REQUIRED' }
    ]});
    return;
  }
  file = {
    name: req.body.name || req.file.originalname,
    path: req.file.path
  };
  model.add(file, function(){
    res.redirect('/attachment/mgmt');
  })
}

function download(req, res){
  var dir = path.join(__dirname, '..', '..');
  model.get(req.params.id, function(result){
    var file = result;
    res.download(path.join(dir, file.path), file.name);
  });
}

function mgmt(req, res){
  model.getAll(function(result){
    res.render('attachment-mgmt.ejs', {title: '添付ファイルの管理', attachments: result});
  });
}

module.exports = {
  mgmt: mgmt,
  upload: upload,
  download: download
};
