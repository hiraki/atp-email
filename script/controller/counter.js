var model = require('../model/counters.js'),
    path = require('path');
var PUBLIC_DIR = path.join(__dirname, '..', '..', 'public');

function access(type, req, res){
  var uniqueId = req.params.uniqueid,
      userId = req.params.user;

  if(isNaN(Number(userId))){
    res.sendFile(path.join(PUBLIC_DIR, 'html', userId));
    return;
  }

  model.access(type, uniqueId, userId, function(result){
    switch(type){
      case 'mail':
      case 'file':
        res.sendFile(path.join(PUBLIC_DIR, 'image', 'image.jpg'));
        break;
      case 'url':
        res.sendFile(path.join(PUBLIC_DIR, 'html', 'index.html'));
        break;
    }
  });
}

function mailAccess(req, res){
  access('mail', req, res);
}

function fileAccess(req, res){
  access('file', req, res);
}

function urlAccess(req, res){
  access('url', req, res);
}

module.exports = {
  mailAccess: mailAccess,
  urlAccess: urlAccess,
  fileAccess: fileAccess
};
