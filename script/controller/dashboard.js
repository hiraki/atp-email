var model = require('../model/dashboard.js'),
    models = {
      emailClient: require('../model/email-client.js')
    };

function show(req, res){
  model.getCounts(function(counts){
    models.emailClient.get(function(econfig){
      res.render(
        'dashboard.ejs',
        {title: '標的型攻撃メール訓練サービス', count: counts, config: {emailClient: econfig}});
    });
  });
}

module.exports = {
  show: show
};
