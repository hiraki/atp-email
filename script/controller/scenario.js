var config = require('../../config.json');
var db = require('../db.js').db,
    model = require('../model/scenarios.js'),
    models = {
      templates: require('../model/templates.js'),
      attachments: require('../model/attachments.js'),
      emailGroups: require('../model/email-groups.js')
    },
    mail = require('../mail.js'),
    path = require('path');

function exec(id){
  sendMail(id, config.mail.interval);
}

function createURL(type, uniqueId, userId){
  return [
    'http://' + config.fqdn + ':' + config.port,
    'open',
    type,
    uniqueId,
    userId
  ].join('/');
}

function sendMail(scenarioId, delay){
  var mailcontent = {};

  model.get(scenarioId, function(s){

    mailcontent = {
      from: s.source,
      subject: s.template.subject
    };
    if(s.attachment.name && s.attachment.path){
      mailcontent.attachments = [{
        fileName: s.attachment.name,
        filePath: path.join(__dirname, '..', '..', s.attachment.path)
      }];
    }
    switch(s.template.format){
      case 'plain':
        mailcontent.text = s.template.text;
        break;
      case 'HTML':
        mailcontent.html = s.template.text;
        break;
    }

    s.group.users.forEach(function(user, i){
      setTimeout(function(){
        var sent = {};
        mailcontent.to = user.email;
        mailcontent.renderData = user;
        mailcontent.renderData.mail = createURL('mail', s.uniqueid, user.id);
        mailcontent.renderData.url = createURL('url', s.uniqueid, user.id);
        mailcontent.renderData.file = createURL('file', s.uniqueid, user.id);
        mail.send(mailcontent, function(err, info){
          sent = {
            scenario: s.id,
            user: user.id,
            attempt: s.attempt + 1,
            sent: new Date()
          };
          if(err){
            console.log(err);
            sent.result = 'error';
          }else{
            console.log(info);
            sent.result = 'success';
          }
          model.addSentResult(sent);
        });
      }, i * delay);
    });

    model.updateAttempt(scenarioId);
  });
}

function sendConfirm(req, res){
  var id = req.params.id;
  mail.verify(function(error, success){
    if(error){
      res.render('error.ejs', {
        title: 'エラー',
        errors: [
          { name: 'MAILCLIENT_VERIFY_FAILED' }
        ]});
    }else{
      model.get(id, function(s){
        res.render('scenario-send-confirm.ejs',
          {title: 'シナリオ実行確認',
            scenario: s, attachment: s.attachment, template: s.template, group: s.group});
      });
    }
  });
}

function add(req, res){
  var name = req.body.name;

  if(name === ''){
    res.render('error.ejs', {
      title: 'エラー',
      errors: [
        {name: 'INPUT_REQUIRED', value: ['シナリオ名']}
      ]});
    return;
  }
  model.add(req.body, function(){
    res.redirect('/scenario/mgmt');
  });
}

function send(req, res){
  var id = req.body.scenarioId;
  exec(id);
  res.redirect('/scenario/' + id + '/sent');
}

function sent(req, res){
  var id = req.params.id;
  model.getHistories(id, function(hists){
    res.render('scenario-send-result.ejs', {title: '送信履歴', histories: hists});
  });
}

function mgmt(req, res){
  model.getAll(function(s){
    models.templates.getAll(function(t){
      models.attachments.getAll(function(a){
        models.emailGroups.getAll(function(g){
          res.render('scenario-mgmt.ejs', {
            title: 'シナリオの管理',
            scenarios: s,
            templates: t,
            attachments: a,
            groups: g
          });
        });
      });
    });
  });
}

module.exports = {
  add: add,
  send: send,
  sent: sent,
  exec: exec,
  mgmt: mgmt,
  initMailer: mail.init,
  sendConfirm: sendConfirm
};

mail.init();
