var cronFactory = require('../model/cron.js').cronFactory,
    scenario = require('./scenario.js'),
    model = {
      cron: cronFactory('cron'),
      at: cronFactory('at')
    },
    models = {
      scenarios: require('../model/scenarios.js')
    },
    cron = require('../cron-jobs.js'),
    util = require('../util.js');

function mgmt(req, res){
  model.cron.getAll(function(c){
    model.at.getAll(function(a_res){
      var a = a_res.map(function(d){
        d.date = util.getLocalString(d.date);
        return d;
      });
      models.scenarios.getAll(function(s){
        var t = new Date();
        res.render('schedule-mgmt.ejs', {
          title: 'スケジュールの管理',
          cron: c,
          at: a,
          scenarios: s,
          today: {
            year: t.getFullYear(),
            month: t.getMonth() + 1,
            day: t.getDate(),
            hour: t.getHours(),
            minute: t.getMinutes()
          }
        });
      });
    });
  });
}

function addCron(scenarioId, config, callback){
  callback = callback || function(){};
  var schedule = {
    type: 'cron',
    scenario: scenarioId,
    config: config,
    exec: function(){scenario.exec(scenarioId);}
  };

  cron.add(schedule, function(err){
    if(err){
      res.render('error.ejs', {
        title: 'エラー',
        errors: [
      {name: err}
      ]});
      return;
    }
    callback(true);
  });
}

function addAt(scenarioId, date, callback){
  callback = callback || function(){};
  if(date.toString() === 'Invalid Date'){
    res.render('error.ejs', {
      title: 'エラー',
      errors: [
    {name: 'INVALID_DATE'}
    ]});
    return;
  }
  var schedule = {
    type: 'at',
    scenario: scenarioId,
    date: date,
    exec: function(){scenario.exec(scenarioId);}
  };
  cron.add(schedule, function(err){
    if(err){
      res.render('error.ejs', {
        title: 'エラー',
        errors: [
      {name: err}
      ]});
      return;
    }
    callback(true);
  });
}

function add(req, res){
  var scenarioId = req.body.scenarioId,
      type = req.body.schedule_type;

  if(type === 'cron'){
    var config = req.body.config_cron.map(function(c){
      return c.length > 0 ? c : '*';
    }).join(' ');
    addCron(scenarioId, config, function(){
      res.redirect('/schedule/mgmt');
    });
    return;

  }else if(req.body.schedule_type === 'at'){
    var config = req.body.config_at;
    var date = new Date(
        config[1] + '/' + config[2] + '/' + config[0] + ' ' + config[3] + ':' + config[4]);
    addAt(scenarioId, date, function(){
      res.redirect('/schedule/mgmt');
    });
    return;
  }
}

function operate(req, res){
  var cmd = req.body.cmd,
      scenarioId = req.params.scenarioId,
      type = req.params.type,
      m;

  switch(type){
    case 'cron':
      m = model.cron;
      break;
    case 'at':
      m = model.at;
      break;
    default:
      res.send('invalid type: ' + req.params.type);
      return;
  }
  m.get(scenarioId, function(result){
    if(result){
      switch(cmd){
        case 'start':
          cron.start(scenarioId, type);
          break;
        case 'stop':
          cron.stop(scenarioId, type);
          break;
        case 'delete':
        case 'remove':
          cron.remove(scenarioId, type);
          break;
        default:
      }
      res.redirect('/schedule/mgmt');
    }else{
      res.send('No cron jobs for scenario: ', req.params.scenario);
    }
  });
}

module.exports = {
  add: add,
  operate: operate,
  mgmt: mgmt
};
