var model = require('../model/email-client.js'),
    mail = require('../mail.js');

function getConfig(req, res){
  model.get(function(item){
    res.render("config.ejs", {title: '設定', config: {emailClient: item}});
  });
}

function setConfig(req, res){
  model.modify(req.body, function(){
    mail.init();
    res.redirect('/');
  });
}

module.exports = {
  getConfig: getConfig,
  setConfig: setConfig
};
