var Iconv = require('iconv').Iconv,
    model = require('../model/statistics.js'),
    models = {
      scenarios: require('../model/scenarios.js')
    };

function mgmt(req, res){
  model.getFormatStat(req.params.scenario, function(statslist){
    res.render('statistics-mgmt.ejs', {title: '統計情報', statistics: statslist});
  });
}

function download(req, res){
  var csv = '';
  models.scenarios.getAll(function(sce){
    model.getAllFormatStats(sce, function(statslist){
      statslist.forEach(function(stat, i){
        if(i === 0){
          csv += [
            'シナリオID',
            'シナリオ名',
            '姓',
            '名',
            'email',
            'メール開封日時',
            'メール開封回数',
            'URLクリック日時',
            'URLクリック回数',
            'ファイル開封日時',
            'ファイル開封回数',
          ].join(',');
        }
        csv += '\r\n';
        csv += [
          stat.scenarioId,
          stat.scenarioName,
          stat.lastname,
          stat.firstname,
          stat.email,
          stat.mailaccessed,
          stat.mailcount,
          stat.urlaccessed,
          stat.urlcount,
          stat.fileaccessed,
          stat.filecount
        ].join(',');
      });
      res.format({
        'text/text': function(){
          var sjis = new Iconv('UTF-8', 'Shift_JIS');
          res.send(sjis.convert(csv));
        }
      });
    });
  });
}

module.exports = {
  mgmt: mgmt,
  download: download
};
