var model = require('../model/users.js'),
    util = require('../util.js'),
    jschardet = require('jschardet'),
    Iconv = require('iconv').Iconv,
    fs = require('fs');
var uploadQueue = [];

function add(req, res){
  var email = req.body.email,
      user = req.body,
      adds = [], dups = [], errs = [];

  if(email === ''){
    res.render('error.ejs', {
      title: 'エラー',
      errors: [
        {name: 'INPUT_REQUIRED', value: ['email']}
    ]});
    return;
  }

  model.duplicate(email, function(dup){
    user.id = 0;
    uploadQueue[0] = user;
    if(dup){
      dups.push(user);
    }else if(!util.isEmail(email)){
      errs.push(user);
    }else{
      adds.push(user);
    }
    res.render('user-add-confirm.ejs',
      {title: 'ユーザーの追加確認', adds: adds, dups: dups, errs: errs, config: {}});
  });
}

function queue(req, res){
  var data, userData, users;
  var loaded = 0, saved = 0, failed = 0;
  var detectResult, iconv;

  uploadQueue = {};

  if(!req.file) {
    res.send('ファイルが不正です');
    return;
  }
  data = fs.readFileSync(req.file.path);
  detectResult = jschardet.detect(data);
  if(detectResult.encoding === 'UTF-8'){
    iconv = new Iconv(detectResult.encoding, 'UTF-8//TRANSLIT//IGNORE');
  }else{
    iconv = new Iconv('Shift_JIS', 'UTF-8//TRANSLIT//IGNORE');
  }
  userData = iconv.convert(data).toString().trim().split('\n');
  loaded = userData.length;

  users = userData.map(function(line){
    var d = line.trim().split(',');
    return {
        firstname: d[1],
        lastname: d[0],
        email: d[2],
        division1: d[3],
        division2: d[4]
    };
  });

  model.getAll(function(existUsers){
  var dups = [], adds = [], errs = [];
    users.forEach(function(u1, i){
      if(!u1){return;}
      u1.id = i;
      if(!util.isEmail(u1.email)){
        errs.push(u1);
        return;
      }
      adds.push(u1);
      uploadQueue[i] = u1;
      existUsers.forEach(function(u2){
        if(u1.email === u2.email){
          var dup = adds.pop();
          if(!dup){return;}
          dups.push(dup);
          return;
        }
      });
    });

    res.render(
      'user-add-confirm.ejs',
      {title: 'ユーザーの追加確認', adds: adds, dups: dups, errs: errs, config: {}});
  });
}

function addFromQueue(req, res){
  var addUserIds = req.body.addId ? [].concat(req.body.addId) : [],
      updateUserIds = req.body.modId ? [].concat(req.body.modId) : [],
      addList = [], updateList = [];

  addUserIds.forEach(function(i){
    addList.push(uploadQueue[i]);
  });
  updateUserIds.forEach(function(i){
    updateList.push(uploadQueue[i]);
  });
  model.addAll(addList, function(){
    model.modifyAll(updateList, function(){
      res.redirect('/user/mgmt');
    });
  });
}

function mgmt(req, res){
  model.getAll(function(list){
    res.render('user-mgmt.ejs', {title: '宛先の管理', users: list, config: {}});
  });
}

module.exports = {
  add: add,
  mgmt: mgmt,
  queue: queue,
  addFromQueue: addFromQueue
};
