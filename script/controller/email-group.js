var model = require('../model/email-groups.js'),
    models = {
      users: require('../model/users.js')
    };

function mgmtAll(req, res){
  model.getAll(function(result){
    var div1 = [], div2 = [];
    models.users.getAll(function(users){
      users.forEach(function(u){
        if(div1.indexOf(u.division1) === -1){
          div1.push(u.division1);
        }
        if(div2.indexOf(u.division2) === -1){
          div2.push(u.division2);
        }
      });
      res.render('emailgroup-mgmt.ejs',
        {title: '宛先グループの管理', groups: result, div1: div1, div2: div2});
    });
  });
}

function mgmt(req, res){
  var name = req.params.name;
  model.get(name, function(result){
    var users = result.users;
    res.render('emailgroup-show.ejs', {title: name, users: users, config: {}});
  });
}

function add(req, res){
  var name = req.body.name,
      ids = req.body.userId ? [].concat(req.body.userId) : [],
      emails = [];

  models.users.getSome(ids, function(userList){
    emails = userList.map(function(u){
      return {
        name: name,
        email: u.email
      };
    });
    model.count(name, function(count){
      if(count > 0){
        res.render('error.ejs', {
          title: 'エラー',
          errors: [
            {name: 'INPUT_DUPLICATED', value: ['グループ名']}
          ]});
        return;
      }
      model.addAll(emails, function(){
        res.redirect('/email_group/mgmt');
      });
    });
  });
}

function addConfirm(req, res){
  var name = req.body.name,
      division1 = req.body.division1,
      division2 = req.body.division2,
      excluded = req.body.userId || [];

  if(name === ''){
    res.render('error.ejs', {
      title: 'エラー',
      errors: [
        {name: 'INPUT_REQUIRED', value: ['グループ名']}
      ]});
    return;
  }

  if(typeof excluded === 'string'){ excluded = [excluded] }
  excluded = excluded.map(function(e){ return Number(e); });

  models.users.getAll(function(result){
    var div1 = division1 ? [].concat(division1) : [],
        div2 = division2 ? [].concat(division2) : [],
        filteredUsers = [];

    result.forEach(function(user){
      if(div1.indexOf(user.division1) !== -1 || div2.indexOf(user.division2) !== -1){
        filteredUsers.push(user);
      }
    });
    res.render('emailgroup-add-confirm.ejs',
      {title: 'グループ追加の確認',
        name: name,
        division1: div1,
        division2: div2,
        users: filteredUsers,
        config: {checkbox: true, checkboxDefaultChecked: true}});
  });
}

module.exports = {
  mgmtAll: mgmtAll,
  mgmt: mgmt,
  add: add,
  addConfirm: addConfirm
};
