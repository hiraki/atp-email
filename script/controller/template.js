var model = require('../model/templates.js');
    operator = require('../operator.js'),
    post = operator.post;

function mgmt(req, res){
  model.getAll(function(result){
    res.render('template-mgmt.ejs', {
      title: 'テンプレートの管理',
      templates: result,
      data: {name: '', subject: '', text: '', id: 0, post: 'add'}});
  });
}

function operateGet(req, res){
  var cmd = req.params.cmd;
  if(['modify', 'duplicate', 'remove'].indexOf(cmd) === -1){
    res.redirect('/template/mgmt');
    return;
  }
  model.getAll(function(list){
    var data;
    for(var i=0; i<list.length; i++){
      if(req.params.id == list[i].id){
        data = list[i];
        break;
      }
    }
    data.post = cmd;
    res.render(
      'template-mgmt.ejs', {title: '雛形の管理', templates: list, data: data});
  });
}

function operatePost(req, res){
  var name = req.body.name;
  if(name === ''){
    res.render('error.ejs', {
      title: 'エラー',
      errors: [
        {name: 'INPUT_REQUIRED', value: ['テンプレート名']}
      ]});
  }else{
    post(model, req.params.cmd, req.body, function(){
      res.redirect('/template/mgmt');
    });
  }
}

module.exports = {
  operate: {
    get: operateGet,
    post: operatePost
  },
  mgmt: mgmt
};
