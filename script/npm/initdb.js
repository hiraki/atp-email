var mysql = require('mysql'),
    path = require('path'),
    config = require('../../config.json'),
    fs = require('fs');

var DB_NAME = config.db.database;
var TEMPLATES_DIR = 'static/templates',
    ATTACHMENTS_DIR = 'static/attachments';

if(config.production === 'true'){
  console.log('!!! production mode !!!')
  console.log('failed...')
  return false;
}

var connection = mysql.createConnection(config.db);

// users
connection.query(
    'create table if not exists users' +
    '(id int(10) AUTO_INCREMENT,' +
    'firstname varchar(100),'+
    'lastname varchar(100),'+
    'email varchar(100) unique,'+
    'company varchar(100),'+
    'division1 varchar(100),'+
    'division2 varchar(100),'+
    'division3 varchar(100),'+
    'modified timestamp,'+
    'PRIMARY KEY (id))'
);

// scenarios
connection.query(
    'create table if not exists scenarios' +
    '(id int(10) AUTO_INCREMENT,' +
    'name varchar(100),'+
    'modified timestamp default current_timestamp on update current_timestamp,'+
    'template int(10) default 1,'+
    'attachment int(10) default 1,'+
    'source varchar(100),'+
    'email_group varchar(100),'+
    'attempt int(10) default 0,'+
    'uniqueid varchar(100),'+
    'PRIMARY KEY (id))'
);

// cron
connection.query(
    'create table if not exists cron' +
    '(id int(10) AUTO_INCREMENT,' +
    'modified timestamp default current_timestamp on update current_timestamp,'+
    'status varchar(100),'+
    'scenario int(10),' +
    'count int(10),' +
    'sent datetime,' +
    'config varchar(100),' +
    'PRIMARY KEY (id))'
);

// at
connection.query(
    'create table if not exists at' +
    '(id int(10) AUTO_INCREMENT,' +
    'modified timestamp default current_timestamp on update current_timestamp,'+
    'status varchar(100),'+
    'count int(10),' +
    'sent datetime,' +
    'scenario int(10),' +
    'date datetime,' +
    'PRIMARY KEY (id))'
);

// counters
connection.query(
    'create table if not exists counters' +
    '(id INT(10) AUTO_INCREMENT, '+
    'scenario INT(10),'+
    'attempt INT(10),'+
    'user INT(10),'+
    'type varchar(100),' +
    'accessed timestamp,' +
    'PRIMARY KEY (id))'
);

// templates
connection.query(
    'create table if not exists templates' +
    '(id INT(10) AUTO_INCREMENT, '+
    'name varchar(100), '+
    'category1 varchar(100), '+
    'category2 varchar(100), '+
    'subject varchar(100), '+
    'text text, '+
    'format varchar(100), '+
    'modified timestamp, '+
    'PRIMARY KEY (id))'
);

// attachments
connection.query(
    'create table if not exists attachments' +
    '(id INT(10) AUTO_INCREMENT, '+
    'name varchar(100), '+
    'path varchar(100), '+
    'modified timestamp default current_timestamp on update current_timestamp,'+
    'PRIMARY KEY (id))'
);

// emailgroups
connection.query(
    'create table if not exists email_groups' +
    '(id INT(10) AUTO_INCREMENT, '+
    'name varchar(100), '+
    'email varchar(100), ' +
    'modified timestamp default current_timestamp on update current_timestamp,'+
    'PRIMARY KEY (id))'
);

// email client
connection.query(
    'create table if not exists email_client' +
    '(id INT(10) AUTO_INCREMENT, '+
    'host varchar(100), '+
    'port int(10), ' +
    'secure varchar(100), ' +
    'user varchar(100), ' +
    'pass varchar(100), ' +
    'modified timestamp default current_timestamp on update current_timestamp,'+
    'PRIMARY KEY (id))'
);

// sent history
connection.query(
    'create table if not exists sent_history' +
    '(id INT(10) AUTO_INCREMENT, '+
    'scenario int(10), ' +
    'user int(10), ' +
    'attempt int(10), ' +
    'result varchar(100), ' +
    'sent timestamp default current_timestamp,'+
    'PRIMARY KEY (id))'
);

// email client dummy settings
connection.query(
    'insert into email_client set ?',
    {
      host: '',
      port: 465,
      secure: 'true',
      user: '',
      pass: ''
    },
    function(err, result){
      if(err){console.log(err);}
    }
);

// include static attachments
function includeStaticAttachments(){
  var att = fs.readdirSync(ATTACHMENTS_DIR, function(err){
    if(err){console.log(err);}
  });

  var query = '';
  var items = att.map(function(file){
    query += 'insert into attachments set ?;';
    return {
      name: file,
      path: path.join(ATTACHMENTS_DIR, file)
    }
  });
  connection.query(query, items, function(err, result){
    if(err){console.log(err);}
  });
}

// include static templates
function includeStaticTemplates(){
  var absoluteDir = path.join(__dirname, '../..', TEMPLATES_DIR),
       list = require(path.join(absoluteDir, 'config.json')),
       query = '', items = [];
  list.forEach(function(t){
    var files = fs.readdirSync(path.join(absoluteDir, t.dir));
    files.forEach(function(f, i){
      var str = fs.readFileSync(path.join(absoluteDir, t.dir, f), 'utf-8');
      var strArray = str.match(/[^\n]+/g);
      var name = strArray.shift(), subject = strArray.shift(), text = strArray.join('\n');
      query += 'insert into templates set ?;';
      items.push({
        name: name,
        category1: t.category1,
        category2: t.category2,
        subject: subject,
        format: 'HTML',
        text: text
      });
    });
  });
  connection.query(query, items, function(err, result){
    if(err){console.log(err);}
  });
}

includeStaticAttachments();
includeStaticTemplates();

(function(){
  for(var i=1; i<=3; i++){
    setTimeout(function(){
      console.log('.');
    }, i * 1000);
  }
})();

setTimeout(function(){
  console.log('done.');
  connection.end();
}, 3000);
