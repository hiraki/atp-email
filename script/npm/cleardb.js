var mysql = require('mysql'),
    config = require('../../config.json');
var DB_NAME = config.db.database;

var connection = mysql.createConnection(config.db);

if(config.production === 'true'){
  console.log('!!! production mode !!!')
  console.log('failed...')
  return false;
}

connection.query('show tables', function(err, result){
  if(err){console.log(err);}
  var query = '';
  result.forEach(function(t){
    query += 'drop table ' + t['Tables_in_' + DB_NAME] + ';';
  });
  connection.query(query, function(err, result){
    if(err){console.log(err);}
    connection.destroy();
  })
});
