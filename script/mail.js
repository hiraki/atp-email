var nodemailer = require('nodemailer'),
    path = require('path'),
    model = require('./model/email-client.js'),
    config = require('../config.json'),
    fs = require('fs');

var transporter = {};

function init(){
  model.get(function(result){
    var smtpConfig = {
      host: result.host,
      port: result.port,
      secure: result.secure,
      auth: {
        user: result.user,
        pass: result.pass
      },
      replyTo: config.mail.replyTo
    };
    transporter = nodemailer.createTransport(smtpConfig);
  });
}

function verify(callback){
  callback = callback || function(){};
  init();
  transporter.verify(callback);
};

function send(options, callback){
  var mailOptions = {
    from: options.from,
    to: options.to,
    replyTo: config.mail.replyTo,
    subject: options.subject,
    text: options.text,
    html: options.html
  };

  if(options.renderData){
    mailOptions.html = renderText(options.html, options.renderData, true);
    mailOptions.text = renderText(options.text, options.renderData, false);
  }

  if(mailOptions.html){
    mailOptions.html = mailOptions.html.replace('\n', '<br>');
  }

  if(options.attachments){
    mailOptions.attachments = [];
    options.attachments.forEach(function(a){
      mailOptions.attachments.push({
        filename: a.fileName,
        content: renderFile(a.filePath, options.renderData),
      });
    });
  }

  transporter.sendMail(mailOptions, callback);
};

function renderText(text, data, isHTML){
  var replaced = String(text);
  if(!text || text.length === 0){ return text; }
  for(key in data){
    var regex = new RegExp('%' + key + '%', 'g');
    replaced = replaced.replace(regex, data[key]);
  }
  if(isHTML){
    var regex = new RegExp('\n', 'g');
    replaced = replaced.replace(regex, '<br />');
  }
  return replaced;
};

function renderFile(filePath, data){
  var content = fs.readFileSync(filePath, 'utf-8');
  return renderText(content, data, false);
};

module.exports.init = init;
module.exports.send = send;
module.exports.verify = verify;

init();
