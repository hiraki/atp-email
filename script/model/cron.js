var db = require('../db.js').db;

function Cron(dbname){
  this.db = dbname;
}

function serialize(dbname, data){
  var set =  {
    scenario: data.scenario,
    status: data.status,
  };
  switch(dbname){
    case 'cron':
      set.config = data.config;
      break;
    case 'at':
      set.date = data.date;
      break;
  }
  return set;
}

function deserialize(dbname, data){
  var set =  {
    scenario: {
      id: data.scenarioId,
      name: data.scenarioName
    },
    status: data.status,
    count: data.count,
    sent: data.sent,
  };
  switch(dbname){
    case 'cron':
      set.config = data.config;
      break;
    case 'at':
      set.date = data.date;
      break;
  }
  return set;
}

Cron.prototype.get = function get(scenarioId, callback){
  callback = callback || function(){};
  var dbname = this.db;
  var query =
    'select ' +
    'scenarios.id as scenarioId, scenarios.name as scenarioName, ' +
    this.db + '.* ' +
    'from ' + this.db + ' ' +
    'join scenarios on scenarios.id = ' + this.db + '.scenario ' +
    'where ?;';

  db(function(connection){
    connection.query(query, {scenario: scenarioId}, function(err, result){
      if(err){console.log(err);}
      var r = result[0];
      callback(deserialize(dbname, r));
      connection.destroy();
    });
  });
}

Cron.prototype.getAll = function getAll(callback){
  callback = callback || function(){};
  var dbname = this.db;
  var query =
    'select ' +
    'scenarios.id as scenarioId, scenarios.name as scenarioName, ' +
    this.db + '.* ' +
    'from ' + this.db + ' ' +
    'join scenarios on scenarios.id = ' + this.db + '.scenario ';

  db(function(connection){
    connection.query(query, function(err, result){
      if(err){console.log(err);}
      callback(result.map(function(r){
        return deserialize(dbname, r);
      }));
      connection.destroy();
    });
  });
}

Cron.prototype.add = function add(data, callback){
  callback = callback || function(){};
  var query = 'insert into ' + this.db + ' set ?;';
  var set = serialize(this.db, data);

  db(function(connection){
    connection.query(query, set, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

Cron.prototype.remove = function remove(scenarioId, callback){
  callback = callback || function(){};
  var query = 'delete from ' + this.db + ' where ?;';
  var where = {scenario: scenarioId};

  db(function(connection){
    connection.query(query, where, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

Cron.prototype.removeAll = function removeAll(callback){
  callback = callback || function(){};
  var query = 'delete from ' + this.db;

  db(function(connection){
    connection.query(query, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

Cron.prototype.changeStatus = function changeStatus(scenarioId, type, newStatus, callback){
  callback = callback || function(){};
  var query = 'update ' + this.db + ' set ? where ?';
  var set = {status: newStatus};
  var where = {scenario: scenarioId};

  db(function(connection){
    connection.query(query, [set, where], function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

function cronFactory(dbname){
  switch(dbname){
    case 'cron':
    case 'at':
      return new Cron(dbname);
    default:
      return new Cron('cron');
  }
}

module.exports.cronFactory = cronFactory;
