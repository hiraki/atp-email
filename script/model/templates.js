var db = require('../db.js').db;

function add(template, callback){
  callback = callback || function(){};
  var query = 'insert into templates set ?;';

  db(function(connection){
    connection.query(query, template, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

function get(id, callback){
  callback = callback || function(){};
  var query = 'select * from templates where ?;';

  db(function(connection){
    connection.query(query, {id: id}, function(err, result){
      if(err){console.log(err);}
      var r = result[0];
      callback({
        id: r.id,
        name: r.name,
        category1: r.category1,
        category2: r.category2,
        subject: r.subject,
        text: r.text,
        format: r.format
      });
      connection.destroy();
    });
  });
}

function getAll(callback){
  callback = callback || function(){};
  var query = 'select * from templates';

  db(function(connection){
    connection.query(query, function(err, result){
      if(err){console.log(err);}
      callback(result.map(function(r){
        return {
          id: r.id,
          name: r.name,
          category1: r.category1,
          category2: r.category2,
          subject: r.subject,
          text: r.text,
          format: r.format
        };
      }));
      connection.destroy();
    });
  });
}

function modify(template, callback){
  callback = callback || function(){};
  var query = 'update templates set ? where ?;';

  db(function(connection){
    connection.query(query, [template, {id: template.id}], function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

module.exports.get = get;
module.exports.add = add;
module.exports.getAll = getAll;
module.exports.modify = modify;
