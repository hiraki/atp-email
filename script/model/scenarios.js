var db = require('../db.js').db,
    util = require('../util.js'),
    uuid = require('node-uuid'),
    emailGroups = require('./email-groups.js'),
    attachments = require('./attachments.js');

function serialize(scenario){
  var data = {
    name: scenario.name,
    source: scenario.from,
    template: scenario.templateId,
    email_group: scenario.groupId,
    uniqueid: uuid.v1().split('-').join('')
  };
  if(scenario.hasAttachment){
    data.attachment = scenario.attachmentId;
  }else{
    data.attachment = null;
  }
  return data;
}

function deserialize(scenario, group, attachment){
  return {
    id: scenario.id,
    uniqueid: scenario.uniqueid,
    name: scenario.name,
    source: scenario.source,
    attempt: scenario.attempt,
    template: {
      id: scenario.templateId,
      name: scenario.templateName,
      subject: scenario.subject,
      text: scenario.text,
      format: scenario.format,
    },
    attachment: {
      id: attachment.id,
      name: attachment.name,
      path: attachment.path
    },
    group: group
  };
}

function get(id, callback){
  callback = callback || function(){};
  var query =
    'select scenarios.*, ' +
    'templates.id as templateId, templates.name as templateName, ' +
    'templates.subject, templates.text, templates.format ' +
    'from scenarios ' +
    'join templates on scenarios.template = templates.id ' +
    'where ?';
  var where = {'scenarios.id': id};

  db(function(connection){
    connection.query(query, where, function(err, result1){
      if(err){console.log(err);}
      var s = result1[0];
      emailGroups.get(s.email_group, function(result2){
        var g = result2;
        attachments.get(s.attachment, function(result3){
          var a = result3;
          callback(deserialize(s, g, a));
        })
      });
    });
  });
}

function getAll(callback){
  callback = callback || function(){};
  var query =
    'select scenarios.*, ' +
    'templates.id as templateId, templates.name as templateName, ' +
    'templates.subject, templates.text, templates.format ' +
    'from scenarios ' +
    'join templates on scenarios.template = templates.id ';

  db(function(connection){
    connection.query(query, function(err, result1){
      if(err){console.log(err);}
      var allScenarios = [];

      if(result1.length === 0){
        callback(allScenarios);
      }
      var i = 0;
      result1.forEach(function(s){
        emailGroups.get(s.email_group, function(result2){
          var g = result2;
          allScenarios.push(deserialize(s, g, {}));
          if(++i === result1.length){
            callback(allScenarios);
            connection.destroy();
          }
        });
      });
    });
  });
}

function getHistories(id, callback){
  var query =
    'select ' +
    'scenarios.name as scenarioName, ' +
    'users.lastname, users.firstname, users.email, ' +
    'sent_history.attempt, sent_history.result, sent_history.sent ' +
    'from sent_history ' +
    'join users on sent_history.user=users.id ' +
    'join scenarios on sent_history.scenario=scenarios.id ' +
    'where ? order by attempt desc';

  db(function(connection){
    connection.query(query, {scenario: id}, function(err, result){
      var hists = [];
      if(err){console.log(err);}
      if(result.length){
        hists = result.map(function(r){
          r.sent = util.getLocalString(r.sent);
          return r;
        });
      }
      connection.destroy();
      callback(hists);
    });
  });
}

function add(scenario, callback){
  callback = callback || function(){};
  var query = 'insert into scenarios set ?';
  var data = serialize(scenario);

  db(function(connection){
    connection.query(query, data, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

function updateAttempt(id, callback){
  callback = callback || function(){};
  var query = 'update scenarios set attempt = attempt + 1 where ?';

  db(function(connection){
    connection.query(query, {id: id}, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

function addSentResult(data, callback){
  callback = callback || function(){};
  var query = 'insert into sent_history set ?;';

  db(function(connection){
    connection.query(query, data, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

module.exports = {
  get: get,
  add: add,
  getAll: getAll,
  getHistories: getHistories,
  updateAttempt: updateAttempt,
  addSentResult: addSentResult
};
