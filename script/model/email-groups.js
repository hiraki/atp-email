var db = require('../db.js').db;

function deserialize(user){
  return {
    id: user.id,
    firstname: user.firstname,
    lastname: user.lastname,
    email: user.email,
    company: user.company,
    division1: user.division1,
    division2: user.division2,
    division3: user.division3
  };
}

function get(name, callback){
  callback = callback || function(){};
  var query = 'select users.*, email_groups.name from email_groups join users on email_groups.email = users.email where ?;';

  db(function(connection){
    connection.query(query, {name: name}, function(err, result){
      if(err){console.log(err);}
      callback({
        name: name,
        users: result.map(function(u){
          return deserialize(u);
        })
      });
      connection.destroy();
    });
  });
}

function getAll(callback){
  callback = callback || function(){};
  var query = 'select users.*, email_groups.name from email_groups join users on email_groups.email = users.email;';

  db(function(connection){
    connection.query(query, function(err, result){
      if(err){console.log(err);}
      var groups = {};
      result.forEach(function(r){
        if(!groups[r.name]) {groups[r.name] = [];}
        groups[r.name].push(deserialize(r));
      });

      callback(groups);
      connection.destroy();
    });
  });
}

function count(name, callback){
  callback = callback || function(){};
  var query = 'select name, count(name) as count from email_groups where ? group by name';

  db(function(connection){
    connection.query(query, {name: name}, function(err, result){
      if(err){console.log(err);}
      var c = result[0];
      if(c){
        callback(Number(c.count));
      }else{
        callback(Number(0));
      }
      connection.destroy();
    });
  });
}

function addAll(users, callback){
  callback = callback || function(){};
  var query = '',
      where = [];
  users.forEach(function(u){
    query += 'insert into email_groups set ?;';
    where.push({name: u.name, email: u.email});
  });

  db(function(connection){
    connection.query(query, where, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

module.exports.get = get;
module.exports.getAll = getAll;
module.exports.addAll = addAll;
module.exports.count = count;
