var db = require('../db.js').db,
    util = require('../util.js');

var type = ['url', 'mail', 'file'];

function empty(scenarioId, callback){
  callback = callback || function(){};

  var query =
    "select  "+
    "users.id, users.email, users.lastname, users.firstname, users.division1, users.division2 "+
    "from scenarios "+
    "join email_groups "+
    "on scenarios.email_group = email_groups.name "+
    "join users "+
    "on email_groups.email = users.email "+
    "where ?";

  db(function(connection){
    var empty = {};
    connection.query(query, {'scenarios.id': scenarioId})
      .on('error', function(err){
        console.log(err);
      })
      .on('result', function(result){
        empty[result.id] = result;
      })
      .on('end', function(){
        connection.destroy();
        callback(empty);
      });
  });
}

function get(scenarioId, callback){
  callback = callback || function(){};
  var query =
    'select counters.user, type, min(accessed) as accessed, count(type) as count from counters where ? group by type, user';
  var ph = {'counters.scenario': scenarioId};

  db(function(connection){
    empty(scenarioId, function(counts){
      connection.query(query, ph)
        .on('error', function(err){
          console.log(err);
        })
        .on('result', function(result){
          if(counts[result.user]){
            counts[result.user][result.type + 'count'] = result.count;
            counts[result.user][result.type + 'accessed'] = result.accessed;
          }
        })
        .on('end', function(){
          connection.destroy();
          callback(counts);
        });
      });
  });
}

function getFormatStat(scenarioId, callback){
  callback = callback || function(){};
  get(scenarioId, function(counts){
    var statslist = [];
    Object.keys(counts).forEach(function(k){
      var count = counts[k];
      Object.keys(count).forEach(function(l){
        if(l.match(/.+accessed/)){
          var date = new Date(count[l]);
          count[l] = util.getLocalString(date);
        }
      });
      statslist.push(count);
    });
    callback(statslist);
  });
}

function getAllFormatStats(scenarios, callback){
  callback = callback || function(){};
  var i = 0, statslist = [];
  scenarios.forEach(function(scenario){
    getFormatStat(scenario.id, function(counts){
      statslist = statslist.concat(counts.map(function(c){
        c.scenarioId = scenario.id;
        c.scenarioName = scenario.name;
        return c;
      }));
      if(++i === scenarios.length){
        callback(statslist);
      }
    });
  });
}

module.exports = {
  get: get,
  getFormatStat: getFormatStat,
  getAllFormatStats: getAllFormatStats
};

