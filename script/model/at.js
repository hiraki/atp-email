var db = require('../db.js').db;

function get(scenarioId, callback){
  callback = callback || function(){};
  var query = 'select * from at where ?;';

  db(function(connection){
    connection.query(query, {scenario: scenarioId}, function(err, result){
      if(err){console.log(err);}
      var r = result[0];
      callback({
        scenario: r.scenario,
        status: r.status,
        count: r.count,
        sent: r.sent,
        date: r.date
      });
      connection.destroy();
    });
  });
}

function getAll(callback){
  callback = callback || function(){};
  var query = 'select * from at;';

  db(function(connection){
    connection.query(query, function(err, result){
      if(err){console.log(err);}
      callback(result.map(function(r){
        return {
          scenario: r.scenario,
          status: r.status,
          count: r.count,
          sent: r.sent,
          date: r.date
        };
      }));
      connection.destroy();
    });
  });
}

function removeAll(callback){
  callback = callback || function(){};
  var query = 'delete from cron;';

  db(function(connection){
    connection.query(query, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

module.exports.get = get;
module.exports.getAll = getAll;
module.exports.removeAll = removeAll;
