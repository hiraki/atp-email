var db = require('../db.js').db;

function getCounts(callback){
  callback = callback || function(){};
  var query =
    'select ' +
    '(select count(*) from scenarios) scenarios,' +
    '(select count(*) from templates) templates,' +
    '(select count(*) from attachments) attachments,' +
    '(select count(*) from at) at,' +
    '(select count(*) from cron) cron,' +
    '(select count(*) from users) users,' +
    '(select count(distinct name) from email_groups) groups';

  db(function(connection){
    var counts = {};
    connection.query(query, function(err, result){
      if(err){console.log(err);}
      counts = result[0];
      Object.keys(counts).map(function(v, i){
        counts[v] = counts[v] > 0 ? counts[v] : '';
      });
      callback(counts);
      connection.destroy();
    });
  });
}

module.exports.getCounts = getCounts;
