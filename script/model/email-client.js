var db = require('../db.js').db;

function serialize(config){
  return {
    host: config.host,
    port: config.port,
    secure: config.secure,
    user: config.user,
    pass: config.pass
  };
}

function deserialize(config){
  return {
    host: config.host,
    port: config.port,
    secure: config.secure,
    user: config.user,
    pass: config.pass
  };
}

function get(callback){
  callback = callback || function(){};
  var query = 'select * from email_client where id = 1;';

  db(function(connection){
    connection.query(query, function(err, result){
      var r = result[0];
      if(err){console.log(err);}
      callback(deserialize(r));
      connection.destroy();
    });
  });
}

function modify(config, callback){
  callback = callback || function(){};
  var query = 'update email_client set ? where id = 1;';
  var data = serialize(config);

  db(function(connection){
    connection.query(query, data, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

module.exports.get = get;
module.exports.modify = modify;
