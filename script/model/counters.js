var db = require('../db.js').db;

function get(scenarioId, callback){
  callback = callback || function(){};
  var query =
    'select counters.*, users.* ' +
    'from counters join users on users.id = counters.user where ?;';

  db(function(connection){
    connection.query(query, {sceanrio: scenarioId}, function(err, result){
      if(err){console.log(err);}
      callback(result.map(function(r){
        return{
          scenario: r.scenario,
          firstname: r.firstname,
          lastname: r.lastname,
          email: r.email,
          company: r.company,
          division1: r.division1,
          division2: r.division2,
          division3: r.division3,
          type: r.type,
          attempt: r.attempt,
          accessed: r.accessed
        };
      }));
      connection.destroy();
    });
  });
}

function add(counter, callback){
  callback = callback || function(){};
  var query = 'insert into counters set ?;';

  db(function(connection){
    connection.query(query, counter, function(err, result){
      if(err){console.log(err);}
      callback(result);
      connection.destroy();
    });
  });
}

function access(type, uniqueid, userid, callback){
  callback = callback || function(){};
  var query = 'select id, attempt from scenarios where ?;';
  var where = {uniqueid: uniqueid};

  if(Number(userid) === NaN){
    callback(false);
    return;
  }

  db(function(connection){
    connection.query(query, where, function(err, result){
      if(err){console.log(err);}
      if(result.length === 0){
        callback();
        return;
      }
      var r = result[0];
      var counter = {
        type: type,
        user: userid,
        scenario: r.id,
        attempt: r.attempt
      };
      add(counter, function(result){
        callback(result);
        connection.destroy();
      });
    });
  });
}

module.exports.get = get;
module.exports.add = add;
module.exports.access = access;
