var db = require('../db.js').db;

function deserialize(attachment){
  return {
    id: attachment.id,
    name: attachment.name,
    path: attachment.path
  };
}

function get(id, callback){
  callback = callback || function(){};
  var query = 'select * from attachments where ?;';

  db(function(connection){
    connection.query(query, {id: id}, function(err, result){
      var r = result[0];
      if(r){
        callback(deserialize(r));
      }else{
        callback([]);
      }
      connection.destroy();
    });
  });
}

function getAll(callback){
  callback = callback || function(){};
  var query = 'select * from attachments;';

  db(function(connection){
    connection.query(query, function(err, result){
      callback(result.map(function(r){
        return deserialize(r);
      }));
      connection.destroy();
    });
  });
}

function add(attachment, callback){
  callback = callback || function(){};
  var query = 'insert into attachments set ?;';

  db(function(connection){
    connection.query(query, attachment, function(err, result){
      callback(result);
      connection.destroy();
    });
  });
}

module.exports.get = get;
module.exports.add = add;
module.exports.getAll = getAll;
