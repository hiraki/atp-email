var db = require('../db.js').db;

function serialize(user){
  return {
    firstname: user.firstname,
    lastname: user.lastname,
    email: user.email,
    company: user.company,
    division1: user.division1,
    division2: user.division2,
    division3: user.division3
  };
}

function deserialize(user){
  return {
    id: user.id,
    firstname: user.firstname,
    lastname: user.lastname,
    email: user.email,
    company: user.company,
    division1: user.division1,
    division2: user.division2,
    division3: user.division3
  }
}

function get(id, callback){
  callback = callback || function(){};
  var query = 'select * from users where ?;';

  db(function(connection){
    connection.query(query, {id: id}, function(err, result){
      var r = result[0];
      callback(deserialize(r));
      connection.destroy();
    });
  });
}

function getSome(ids, callback){
  callback = callback || function(){};
  ids = [].concat(ids);

  var query = 'select * from users where ?',
      where = [];
  ids.forEach(function(id, i){
    if(i !== 0){query += ' or ?';}
    where.push({id: id});
  });

  db(function(connection){
    connection.query(query, where, function(err, result){
      callback(result.map(function(r){
        return deserialize(r);
      }));
      connection.destroy();
    });
  });
}

function getAll(callback){
  callback = callback || function(){};
  var query = 'select * from users';

  db(function(connection){
    connection.query(query, function(err, result){
      callback(result.map(function(r){
        return deserialize(r);
      }));
      connection.destroy();
    })
  });
}

function addAll(users, callback){
  callback = callback || function(){};
  var query = '';
  users.forEach(function(u){
    query += 'insert into users set ?;';
  });
  var data = users.map(function(user){
    return serialize(user);
  });

  db(function(connection){
    connection.query(query, data, function(err, result){
      callback(result);
      connection.destroy();
    });
  });
}

function modifyAll(users, callback){
  callback = callback || function(){};
  var query = '', data = [];
  users.forEach(function(user){
    query += 'update users set ? where ?;';
    data.push(serialize(user));
    data.push({email: user.email});
  });

  db(function(connection){
    connection.query(query, data, function(err, result){
      callback(result);
      connection.destroy();
    });
  });
}

function duplicate(email, callback){
  callback = callback || function(){};
  var query = 'select email from users where ?;';

  db(function(connection){
    connection.query(query, {email: email}, function(err, result){
      if(result.length > 0){
        callback(true);
      }else{
        callback(false);
      }
      connection.destroy();
    });
  });
}

module.exports.get = get;
module.exports.getSome = getSome;
module.exports.getAll = getAll;
module.exports.addAll = addAll;
module.exports.modifyAll = modifyAll;
module.exports.duplicate = duplicate;
